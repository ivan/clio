/*
 *  Copyright (C) 2011, 2014 Tino Kluge (ttk448 at gmail.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TK_SPLINE_H
#define TK_SPLINE_H

#include <QObject>
#include <QList>
#include <QVector>
#include <QColor>

// Spline interpolation
class Spline: public QObject {
    Q_OBJECT

    Q_PROPERTY(QList<double> xs READ xs WRITE setXs NOTIFY xsChanged)
    Q_PROPERTY(QList<double> ys READ ys WRITE setYs NOTIFY ysChanged)

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(double maximumY  READ maximumY  WRITE setMaximumY  NOTIFY maximumYChanged)

public:
    enum Type {
        FirstDerivation = 1,
        SecondDerivation = 2
    };

    // set default boundary condition to be zero curvature at both ends
    Spline(QObject *parent = nullptr);

    // optional, but if called it has to come be before set_points()
    // void setBoundary(Type left, double left_value,
    //                  Type right, double right_value,
    //                  bool force_linear_extrapolation=false);

    void setXs(const QList<double> &xs);
    void setYs(const QList<double> &ys);

    const QList<double> &xs() const;
    const QList<double> &ys() const;

    QColor color() const;
    void setColor(const QColor &color);

    void setMaximumY(double maximumY);
    double maximumY() const;


    double operator() (double x) const;

Q_SIGNALS:
    void xsChanged(const QList<double> &xs);
    void ysChanged(const QList<double> &ys);
    void colorChanged(const QColor &color);
    void maximumYChanged(double maximumY);


private:
    void calculate() const;

    QList<double> m_xs, m_ys;
    QColor  m_color;

    Type m_left, m_right;
    double  m_left_value, m_right_value;
    bool    m_force_linear_extrapolation;

    mutable QVector<double> m_a, m_b, m_c;
    mutable double  m_b0, m_c0;
    mutable bool m_valid;
    double m_maximumY;

};

#endif /* TK_SPLINE_H */
