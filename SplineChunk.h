/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2015  Ivan Čukić <ivan.cukic@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TIMELINECHUNK_H
#define TIMELINECHUNK_H

#include <QQuickItem>

class Spline;

class SplineChunk : public QQuickItem {
    Q_OBJECT

    Q_PROPERTY(Spline* spline    READ spline    WRITE setSpline    NOTIFY splineChanged)
    Q_PROPERTY(double rangeFrom  READ rangeFrom WRITE setRangeFrom NOTIFY rangeFromChanged)
    Q_PROPERTY(double rangeTo    READ rangeTo   WRITE setRangeTo   NOTIFY rangeToChanged)

    Q_PROPERTY(QString paintMode READ paintMode WRITE setPaintMode NOTIFY paintModeChanged)
    Q_PROPERTY(double maximumY   READ maximumY  NOTIFY maximumYChanged)

public:
    SplineChunk(QQuickItem *parent = nullptr);
    ~SplineChunk();

    void setSpline(Spline *spline);
    Spline *spline() const;

    void setRangeFrom(double from);
    double rangeFrom() const;

    void setRangeTo(double to);
    double rangeTo() const;

    void setPaintMode(const QString &paintMode);
    QString paintMode() const;

    double maximumY() const;

Q_SIGNALS:
    void splineChanged(Spline *spline);
    void rangeFromChanged(double from);
    void rangeToChanged(double to);
    void paintModeChanged(const QString &paintMode);
    double maximumYChanged(double maximumY);

protected:
    QSGNode* updatePaintNode(QSGNode*, QQuickItem::UpdatePaintNodeData*) override;


private:
    unsigned short m_segmentCount;
    std::pair<double, double> m_range;
    Spline* m_spline;
    QString m_paintMode;
    double m_maximumY;
};

#endif // TIMELINECHUNK_H
