#include <QtGui>
#include <QtQuick>

#include <QGuiApplication>
#include <QApplication>

#include "SplineChunk.h"
#include "Spline.h"

// int main(int argc, char** argv)
// {
//     QGuiApplication app(argc, argv);
//
//     qmlRegisterType<SplineChunk>("org.kde.clio", 1, 0, "SplineChunk");
//     qmlRegisterType<Spline>("org.kde.clio", 1, 0, "Spline");
//
//     QQuickView view;
//     auto format = view.format();
//     format.setSamples(32);
//     view.setFormat(format);
//     view.setResizeMode(QQuickView::SizeRootObjectToView);
//
//     view.setSource(QUrl::fromLocalFile("qml/main.qml"));
//
//     view.show();
//
//     return app.exec();
// }
//
// #include <QApplication>
#include <QQmlApplicationEngine>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<SplineChunk>("org.kde.clio", 1, 0, "SplineChunk");
    qmlRegisterType<Spline>("org.kde.clio", 1, 0, "Spline");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qml/main.qml")));

    return app.exec();
}

