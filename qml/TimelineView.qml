import QtQuick 2.0
import org.kde.clio 1.0

Rectangle {
    id: root
    width: 600
    height: 400

    color: "#232629"

    property int chunkWidth: width / 10

    Timeline {
        id: timeline2

        width: parent.width
        height: 200
        y : 200

        color: "#da4453"
        // color: "#f67400"

        xs: [ 0, 1, 2, 3, 4, 5,  6, 7, 8, 9, 10 ]
        ys: [ 3, 0, 2, 3, 2, 1,  2, 4, 1, 3, 5 ]

        maximumY : 10
    }

    Timeline {
        id: timeline1

        width: parent.width
        height: 200
        y : 100

        color: "#3daee9"

        xs: [ 0, 1, 2, 3, 4, 5,  6, 7, 8, 9, 10 ]
        ys: [ 0, 0, 1, 0, 2, 10, 5, 4, 2, 4, 3 ]

        maximumY : 10
    }

    Timeline {
        id: timeline3

        width: parent.width
        height: 200
        y : 300

        color: "#2ecc71"

        xs: [ 0, 1, 2, 3, 4, 5,  6, 7,  8, 9, 10 ]
        ys: [ 0, 0, 0, 0, 0, 0,  5, 8, 1, 0, 0 ]

        maximumY : 10
    }
}

