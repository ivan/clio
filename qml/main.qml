import QtQuick 2.0
import org.kde.clio 1.0

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

ApplicationWindow {
    title: qsTr("Hello World")
    width: 640
    height: 480
    visible: true

    toolBar:ToolBar {
        RowLayout {
            anchors.fill: parent
            ToolButton {
                iconSource: "new.png"
            }
            ToolButton {
                iconSource: "open.png"
            }
            ToolButton {
                iconSource: "save-as.png"
            }
            Item { Layout.fillWidth: true }
            CheckBox {
                text: "Enabled"
                checked: true
                Layout.alignment: Qt.AlignRight
            }
        }
    }

    // menuBar: MenuBar {
    //     Menu {
    //         title: qsTr("&File")
    //         MenuItem {
    //             text: qsTr("&Open")
    //             onTriggered: messageDialog.show(qsTr("Open action triggered"));
    //         }
    //         MenuItem {
    //             text: qsTr("E&xit")
    //             onTriggered: Qt.quit();
    //         }
    //     }
    // }

    TimelineView {
        anchors.fill: parent
    }

    MessageDialog {
        id: messageDialog
        title: qsTr("May I have your attention, please?")

        function show(caption) {
            messageDialog.text = caption;
            messageDialog.open();
        }
    }
}

