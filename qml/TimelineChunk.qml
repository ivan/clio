import QtQuick 2.0
import org.kde.clio 1.0

Item {
    property alias spline:    splineChunk.spline
    property alias rangeFrom: splineChunk.rangeFrom
    property alias rangeTo:   splineChunk.rangeTo

    // Rectangle {
    //     color: "#dddddd"
    //     opacity: .5
    //
    //     anchors {
    //         fill: parent
    //         margins: 2
    //     }
    //
    //     z: 0
    // }

    SplineChunk {
        id: splineChunk

        anchors.fill: parent
    }

    // SplineChunk {
    //     id: splineChunkLine

    //     spline:    splineChunk.spline
    //     rangeFrom: splineChunk.rangeFrom
    //     rangeTo:   splineChunk.rangeTo

    //     paintMode: "line"

    //     anchors.fill: parent

    //     z: 2
    // }
}
