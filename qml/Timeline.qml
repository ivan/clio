import QtQuick 2.0
import org.kde.clio 1.0

Item {
    id: root

    property alias color    : splineCurve.color
    property alias xs       : splineCurve.xs
    property alias ys       : splineCurve.ys
    property alias maximumY : splineCurve.maximumY

    property int chunkCount     : 10
    property int chunkWidth     : width / chunkCount

    property double xRange      : xs[xs.length - 1] - xs[0]
    property double xChunkRange : xRange / chunkCount

    Spline {
        id: splineCurve
    }

    Row {
        anchors.fill: parent

        Repeater {
            model: root.chunkCount

            TimelineChunk {
                width     : root.chunkWidth
                height    : parent.height

                spline    : splineCurve
                rangeFrom : xChunkRange * index
                rangeTo   : xChunkRange * (index + 1)
            }
        }

    }

    Rectangle {
        height           : 2
        color            : root.color
        width            : parent.width
        anchors.centerIn : parent
        opacity: .5
    }
}

